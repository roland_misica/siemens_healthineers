# README #

## Tento repozitár obsahuje Flutter aplikáciu pre Android + iOS. ##
## Slúži iba na demonštráciu/PoC možnej produkčnej mobilnej aplikácie ##

## Čo aplikácia vie / čo je implementované ? ##

* Aplikácia načítavá údaje z REST API
* Načítané údaje zálohuje na local storage
* Možnosť aktualizovať údaje s Pull-to-refresh
* Vie zobrazenie aj v Light Theme aj Dark Theme
* Reakcie na vypnuté internetové pripojenie
* Zobrazenie chybových hlášok v prípade vzniknutých chýb

## Čo aplikácie nevie / čo momentálne nie je implementované ? ##
* Filtrovanie, orderovanie zoznamu pacientov
* Search bar nefiltruje podľa zadaných parametrov
* Otváranie detailov o pacientovi
* Žiadne z tlačidiel na vrchnej lište

### Ako to spojazdním ? ###

* Potrebný flutter SDK, Android Studio/Xcode...

### Coding standards ###
* Dart format line length: 120
* All files are formated according dartfrmt
* No Warnings, or Hints in Dart Analysis !!!