import 'package:flutter/material.dart';

import 'src/injection_container.dart' as di;
import 'config.dart';
import 'src/app.dart';

void main() async {
  Config.appFlavor = Flavor.development;

  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  runApp(const PatientApp());
}
