import 'package:equatable/equatable.dart';

class Patient extends Equatable {
  final String id;
  final String? name;
  final String? gender;
  final DateTime? birthDate;

  const Patient({
    required this.id,
    required this.name,
    required this.gender,
    required this.birthDate,
  });

  @override
  List<Object?> get props => [
        id,
        name,
        gender,
        birthDate,
      ];
}
