import 'package:dartz/dartz.dart';

import '../../core/error/failures.dart';

import '../entities/patient.dart';

/// Interface of Repository for Patient data
/// Used for:
/// - receiving patient data from remote source
/// - caching data to local storage
abstract class PatientRepository {
  Future<Either<Failure, List<Patient>>> getPatientsList();
  Future<Either<Failure, List<Patient>>> getPatientsListFromRemote();
}
