import 'package:flutter/material.dart';

import '../config.dart';
import 'injection_container.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'presentation/blocs/patients/patients_bloc.dart';

import 'presentation/pages/home_page.dart';

class PatientApp extends StatelessWidget {
  const PatientApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PatientsBloc>(
      create: (context) => PatientsBloc(patientRepository: sl())..add(PatientsStarted()),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Siemens Patient app',
        theme: Config.lightTheme,
        darkTheme: Config.darkTheme,
        home: const HomePage(),
      ),
    );
  }
}
