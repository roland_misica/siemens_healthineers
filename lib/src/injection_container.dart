import 'package:get_it/get_it.dart';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:http/http.dart' as http;

import 'package:shared_preferences/shared_preferences.dart';

import 'core/utils/network_checker.dart';

import 'data/datasources/local_data_source.dart';
import 'data/datasources/remote_data_source.dart';
import 'data/repositories/patient_repository_impl.dart';

import 'domain/repositories/patient_repository.dart';

import '../config.dart';

final sl = GetIt.instance;

Future<void> init() async {
  // External libraries
  sl.registerLazySingleton(() => Connectivity());
  sl.registerLazySingleton(() => http.Client());
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);

  // Core
  sl.registerLazySingleton<NetworkChecker>(() => NetworkCheckerImpl(connectivity: sl()));

  // Data sources
  sl.registerLazySingleton<LocalDataSource>(() => LocalDataSourceImpl(sharedPreferences: sl()));
  sl.registerLazySingleton<RemoteDataSource>(() => RemoteDataSourceImpl(api: Config.api, client: sl()));

  // Repositories
  sl.registerLazySingleton<PatientRepository>(
    () => PatientRepositoryImpl(
      localDataSource: sl(),
      remoteDataSource: sl(),
      networkChecker: sl(),
    ),
  );
}
