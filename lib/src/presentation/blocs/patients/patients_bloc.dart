import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../core/error/failures.dart';

import '../../../domain/entities/patient.dart';
import '../../../domain/repositories/patient_repository.dart';

part 'patients_event.dart';
part 'patients_state.dart';

class PatientsBloc extends Bloc<PatientsEvent, PatientsState> {
  final PatientRepository patientRepository;

  PatientsBloc({required this.patientRepository}) : super(PatientsInitial()) {
    on<PatientsStarted>(_onPatientsStartedEvent);
    on<PatientsRefreshed>(_onPatientsRefreshedEvent);
  }

  void _onPatientsStartedEvent(PatientsStarted event, Emitter<PatientsState> emit) async {
    emit(PatientsLoadInProgress());
    final patientsListOrFailure = await patientRepository.getPatientsList();
    emit(patientsListOrFailure.fold(
      (failure) => PatientsLoadFailure(message: mapFailureToMessage(failure)),
      (patients) => PatientsLoadSuccess(patients: patients),
    ));
  }

  void _onPatientsRefreshedEvent(PatientsRefreshed event, Emitter<PatientsState> emit) async {
    emit(PatientsLoadInProgress());
    final patientsListOrFailure = await patientRepository.getPatientsListFromRemote();
    emit(patientsListOrFailure.fold(
      (failure) => PatientsLoadFailure(message: mapFailureToMessage(failure)),
      (patients) => PatientsLoadSuccess(patients: patients),
    ));
  }
}
