part of 'patients_bloc.dart';

abstract class PatientsState extends Equatable {
  const PatientsState();
}

class PatientsInitial extends PatientsState {
  @override
  List<Object> get props => [];
}

class PatientsLoadInProgress extends PatientsState {
  @override
  List<Object> get props => [];
}

class PatientsLoadSuccess extends PatientsState {
  final List<Patient> patients;

  const PatientsLoadSuccess({
    required this.patients,
  });

  @override
  List<Object?> get props => [patients];
}

class PatientsLoadFailure extends PatientsState {
  final String message;

  const PatientsLoadFailure({
    required this.message,
  });

  @override
  List<Object?> get props => [message];
}
