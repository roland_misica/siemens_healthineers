part of 'patients_bloc.dart';

abstract class PatientsEvent extends Equatable {
  const PatientsEvent();
}

class PatientsStarted extends PatientsEvent {
  @override
  List<Object?> get props => [];
}

class PatientsRefreshed extends PatientsEvent {
  @override
  List<Object?> get props => [];
}
