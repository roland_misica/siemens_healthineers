import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../blocs/patients/patients_bloc.dart';

import '../widgets/patient_tile.dart';
import '../widgets/patients_filter.dart';
import '../widgets/error_message.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  Future<void> onRefresh(BuildContext context) {
    final patientsBloc = BlocProvider.of<PatientsBloc>(context)..add(PatientsRefreshed());
    return patientsBloc.stream.firstWhere((state) => state is! PatientsLoadInProgress);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.menu),
          onPressed: () {},
        ),
        centerTitle: true,
        title: const Text("CT 1"),
        actions: [
          IconButton(
            icon: const Icon(Icons.add),
            onPressed: () {},
          )
        ],
      ),
      body: SafeArea(
        child: BlocListener<PatientsBloc, PatientsState>(
          listener: (context, state) {
            if (state is PatientsLoadFailure) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  behavior: SnackBarBehavior.floating,
                  backgroundColor: Colors.red,
                  content: Text(state.message),
                ),
              );
            }
          },
          child: BlocBuilder<PatientsBloc, PatientsState>(
            builder: (context, state) {
              if (state is PatientsLoadSuccess) {
                return Column(
                  children: [
                    PatientsFilter(count: state.patients.length),
                    Expanded(
                      child: RefreshIndicator(
                        onRefresh: () => onRefresh(context),
                        child: ListView(
                            children: ListTile.divideTiles(
                          context: context,
                          tiles: state.patients.map((patient) => PatientTile(patient: patient)).toList(),
                        ).toList()),
                      ),
                    ),
                  ],
                );
              }

              if (state is PatientsLoadFailure) {
                return Center(
                  child: ErrorMessage(
                    onRetry: () => onRefresh(context),
                  ),
                );
              }
              // PatientsInitial
              // PatientsLoadInProgress
              return const Center(child: CircularProgressIndicator());
            },
          ),
        ),
      ),
    );
  }
}
