import 'package:flutter/material.dart';

class ErrorMessage extends StatelessWidget {
  final VoidCallback onRetry;

  const ErrorMessage({required this.onRetry, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        const Icon(
          Icons.cancel_outlined,
          size: 128,
          color: Colors.red,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Text(
            "Something went wrong...",
            style: Theme.of(context).textTheme.headline5,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Text(
            "We are going to fix it.",
            style: Theme.of(context).textTheme.headline6!.copyWith(fontWeight: FontWeight.w300),
          ),
        ),
        TextButton(onPressed: onRetry, child: const Text("Retry"))
      ],
    );
  }
}
