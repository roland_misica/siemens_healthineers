import 'package:intl/intl.dart';

import 'package:flutter/material.dart';

import '../../domain/entities/patient.dart';

class PatientTile extends StatelessWidget {
  final Patient patient;

  const PatientTile({required this.patient, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      isThreeLine: true,
      title: Text(
        patient.name ?? "No name",
        overflow: TextOverflow.ellipsis,
        softWrap: false,
        maxLines: 1,
      ),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 5),
            child: Text(
              '*${patient.birthDate != null ? DateFormat('dd/MM/yyyy').format(patient.birthDate!) : "unknown"}, '
              '${patient.id}',
              overflow: TextOverflow.fade,
              softWrap: false,
              maxLines: 1,
            ),
          ),
          Text(
            'CT Abdomen, CT Thorax',
            style: Theme.of(context).textTheme.caption,
          ),
          Text(
            '02:00 PM 18/04/2018',
            style: Theme.of(context).textTheme.caption,
          ),
        ],
      ),
      leading: const Icon(
        Icons.person,
        size: 32,
      ),
      trailing: IconButton(
        icon: const Icon(Icons.more_vert),
        onPressed: () {},
      ),
    );
  }
}
