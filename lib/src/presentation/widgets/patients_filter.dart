import 'package:flutter/material.dart';

class PatientsFilter extends StatelessWidget {
  final int count;

  const PatientsFilter({required this.count, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.all(10),
          child: TextField(
            decoration: InputDecoration(
              isDense: true,
              filled: true,
              contentPadding: EdgeInsets.zero,
              hintText: "Search Patient name, ID, DOB",
              hintStyle: const TextStyle(fontWeight: FontWeight.w300),
              prefixIcon: const Icon(Icons.search),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
                borderSide: const BorderSide(width: 0.1),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Today, All ($count)',
                style: Theme.of(context).textTheme.subtitle1,
              ),
              Row(
                children: [
                  IconButton(
                    icon: const Icon(Icons.filter_list),
                    visualDensity: VisualDensity.compact,
                    onPressed: () {},
                  ),
                  IconButton(
                    icon: const Icon(Icons.filter_list_alt),
                    visualDensity: VisualDensity.compact,
                    onPressed: () {},
                  ),
                ],
              )
            ],
          ),
        ),
        const Divider(
          height: 2,
          thickness: 2,
        ),
      ],
    );
  }
}
