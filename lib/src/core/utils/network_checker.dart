/// Network connectivity checker utility
/// checks for mobile or wifi connection

import 'package:connectivity_plus/connectivity_plus.dart';

/// Interface of Network checker utility
abstract class NetworkChecker {
  Future<bool> get isConnected;
}

/// Implementation of network checker utility
class NetworkCheckerImpl implements NetworkChecker {
  final Connectivity connectivity;

  NetworkCheckerImpl({
    required this.connectivity,
  });

  @override
  Future<bool> get isConnected async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    return connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi;
  }
}
