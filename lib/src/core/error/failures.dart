/// All failure classes, used for UI as an message for user.

import 'package:equatable/equatable.dart';

/// Base class
abstract class Failure extends Equatable {
  @override
  List<Object?> get props => [];
}

/// No internet connection on phone
class InternetConnectionFailure extends Failure {}

/// Default server related failure
class ServerFailure extends Failure {
  final String message;

  ServerFailure(this.message);

  @override
  List<Object?> get props => [message];
}

/// Authentication failure from server (401)
class AuthenticationFailure extends Failure {}

/// Default cache related failure
class CacheFailure extends Failure {}

/// Default failure, when unexpected Exception occurs
class UnknownFailure extends Failure {}

/// Maps Failure object to message shown on UI
/// Can be used with i18l using string keys
String mapFailureToMessage(Failure failure) {
  switch (failure.runtimeType) {
    case InternetConnectionFailure:
      return 'No internet connection';
    case ServerFailure:
      return (failure as ServerFailure).message;
    case AuthenticationFailure:
      return 'Failed authentication to the server';
    case CacheFailure:
      return 'Cache Failure';
    case UnknownFailure:
    default:
      return 'Unexpected error';
  }
}
