import 'package:flutter/foundation.dart';

/// Simulation of Exception reporting system
reportToCrashlytics(Object e, StackTrace stackTrace) {
  if (kDebugMode) {
    print("---EXCEPTION REPORTED---");
    print(e);
    print(stackTrace);
    print("------------------------");
  }
}

/// Simulation of Exception reporting system
reportRemoteExceptionToCrashlytics(String e) {
  if (kDebugMode) {
    print("---EXCEPTION FROM SERVER REPORTED---");
    print(e);
    print("------------------------");
  }
}
