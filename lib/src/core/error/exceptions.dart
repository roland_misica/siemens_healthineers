/// All custom exceptions, which can be thrown inside application

/// Default server exception
class ServerException implements Exception {
  final String message;

  ServerException(this.message);
}

/// Server authentication exception (401)
class AuthenticationException implements Exception {}

/// Default cache exception
class CacheException implements Exception {}
