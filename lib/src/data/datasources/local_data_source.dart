/// Local storage of data retrieved from remote source

import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

import '../../core/error/exceptions.dart';

import '../models/patient_model.dart';

/// Interface of Local data source
abstract class LocalDataSource {
  Future<List<PatientModel>> getPatientsList();
  Future<void> storePatientsList(List<PatientModel> patients);
}

/// Implementation of local data source
class LocalDataSourceImpl implements LocalDataSource {
  // Used as an example, even though it is worst local storage solution due to it's performance :)
  final SharedPreferences sharedPreferences;

  LocalDataSourceImpl({required this.sharedPreferences});

  /// Get patient list from local Storage
  @override
  Future<List<PatientModel>> getPatientsList() async {
    String? patientsJson = sharedPreferences.getString('PATIENT LIST');
    if (patientsJson != null) {
      List<dynamic> patientJsonList = json.decode(patientsJson);
      return patientJsonList.map((patientJson) => PatientModel.fromJson(patientJson)).toList();
    } else {
      throw CacheException();
    }
  }

  /// Store patient list to local Storage
  @override
  Future<void> storePatientsList(List<PatientModel> patients) {
    return sharedPreferences.setString('PATIENT LIST', json.encode(patients));
  }
}
