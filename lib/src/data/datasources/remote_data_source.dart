/// Remote data storage
import 'dart:convert';
import 'package:http/http.dart';

import '../../core/error/exception_logger.dart';
import '../../core/error/exceptions.dart';

import '../models/patient_model.dart';

/// Interface of Remote data storage
abstract class RemoteDataSource {
  Future<List<PatientModel>> getPatientsList();
}

/// Implementation of remote data source
class RemoteDataSourceImpl implements RemoteDataSource {
  final String api;
  final Client client;

  RemoteDataSourceImpl({
    required this.api,
    required this.client,
  });

  Map<String, String> get headers => {
        "content-type": "application/json",
      };

  /// Send unexpected response to the crash reporting system of your choice
  /// e.g. Sentry/Crashlytics/etc..
  void _captureRemoteException(Response response) {
    Map<String, dynamic> report = {
      'request': {
        'method': response.request?.method,
        'url': response.request?.url.toString(),
        'headers': response.request?.headers,
      },
      'response': {
        'statusCode': response.statusCode,
        'headers': response.headers,
        'body': response.body,
      }
    };

    reportRemoteExceptionToCrashlytics(json.encode(report));
  }

  /// Get list of patients
  /// may throw an exception
  @override
  Future<List<PatientModel>> getPatientsList() async {
    Response response = await client.get(
      Uri.parse("$api/Patient"),
      headers: headers,
    );
    switch (response.statusCode) {
      case 200:
        var patientsJson = jsonDecode(response.body);
        var list = patientsJson['entry']
            .map<PatientModel>((patientJson) => PatientModel.fromRemote(patientJson['resource']))
            .toList();
        return list;
      case 401:
        throw AuthenticationException();
      case 500:
      default:
        _captureRemoteException(response);
        throw ServerException(response.body);
    }
  }
}
