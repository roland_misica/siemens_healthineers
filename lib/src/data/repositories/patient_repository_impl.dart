import 'package:dartz/dartz.dart';

import '../../core/error/exception_logger.dart';
import '../../core/error/exceptions.dart';
import '../../core/error/failures.dart';
import '../../core/utils/network_checker.dart';

import '../../data/datasources/local_data_source.dart';
import '../../data/datasources/remote_data_source.dart';

import '../../domain/entities/patient.dart';
import '../../domain/repositories/patient_repository.dart';

/// Implementation of Repository for Patient data
/// Used for:
/// - receiving patient data from remote source
/// - caching data to local storage
class PatientRepositoryImpl implements PatientRepository {
  final LocalDataSource localDataSource;
  final RemoteDataSource remoteDataSource;
  final NetworkChecker networkChecker;

  PatientRepositoryImpl({
    required this.localDataSource,
    required this.remoteDataSource,
    required this.networkChecker,
  });

  @override
  Future<Either<Failure, List<Patient>>> getPatientsList() async {
    // Try load date from local cache, if not, fetch them from remote source
    try {
      List<Patient> patients = await localDataSource.getPatientsList();
      return Right(patients);
    } on CacheException {
      return await getPatientsListFromRemote();
    } catch (error, stackTrace) {
      reportToCrashlytics(error, stackTrace);
      return Left(UnknownFailure());
    }
  }

  @override
  Future<Either<Failure, List<Patient>>> getPatientsListFromRemote() async {
    if (await networkChecker.isConnected) {
      try {
        final patients = await remoteDataSource.getPatientsList();
        localDataSource.storePatientsList(patients);
        return Right(patients);
      } on AuthenticationException {
        return Left(AuthenticationFailure());
      } on ServerException catch (e) {
        return Left(ServerFailure(e.message));
      } catch (error, stackTrace) {
        reportToCrashlytics(error, stackTrace);
        return Left(UnknownFailure());
      }
    } else {
      return Left(InternetConnectionFailure());
    }
  }
}
