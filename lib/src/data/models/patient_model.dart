import 'package:siemens_healthineers/src/domain/entities/patient.dart';

/// Model entity Patient
class PatientModel extends Patient {
  const PatientModel({
    required String id,
    required String? name,
    required String? gender,
    required DateTime? birthDate,
  }) : super(
          id: id,
          name: name,
          gender: gender,
          birthDate: birthDate,
        );

  /// Option to use different data format loaded from remote source
  factory PatientModel.fromRemote(Map<String, dynamic> obj) {
    // API is sometimes broken and sometimes it contains name, sometimes not...
    String name;
    try {
      name = obj['name'][0]['text'];
    } catch (e) {
      name = "No name";
    }

    return PatientModel(
      id: obj['id'],
      name: name,
      gender: obj['gender'],
      birthDate: obj['birthDate'] != null ? DateTime.tryParse(obj['birthDate']) : null,
    );
  }

  /// Constructor for mapping data from JSON to Entity
  factory PatientModel.fromJson(Map<String, dynamic> json) => PatientModel(
        id: json['id'],
        name: json['name'],
        gender: json['gender'],
        birthDate: json['birthDate'] != null ? DateTime.tryParse(json['birthDate']) : null,
      );

  /// Map object to format used on remote source
  /// For now, used JSON format as default
  Map<String, dynamic> toRemote() => toJson();

  /// Map object to JSON
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'gender': gender,
        'birthDate': birthDate?.toIso8601String().split('T').first,
      };
}
