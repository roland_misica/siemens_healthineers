import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

/// Config file for mobile app
/// Useful for flavoured builds, changing API URLs, changing pre built themes, etc...

enum Flavor {
  development,
  production,
  test,
}

class Config {
  static Flavor? appFlavor;

  static String get api {
    switch (appFlavor) {
      case Flavor.development:
      case Flavor.production:
      case Flavor.test:
      default:
        return 'http://hapi.fhir.org/baseR4';
    }
  }

  static ThemeData get lightTheme => ThemeData(
        brightness: Brightness.light,
        primarySwatch: Colors.deepOrange,
        appBarTheme: const AppBarTheme(
          systemOverlayStyle: SystemUiOverlayStyle(
            //Android 6.0+
            statusBarColor: Colors.white,
            statusBarIconBrightness: Brightness.dark,
            systemStatusBarContrastEnforced: false,

            //Android 8.0+
            //systemNavigationBarContrastEnforced: false,
            systemNavigationBarColor: Colors.white,
            systemNavigationBarIconBrightness: Brightness.dark,

            //iOS
            //statusBarBrightness: Brightness.dark,
          ),
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
        ),
        scaffoldBackgroundColor: Colors.white,
      );

  static ThemeData get darkTheme => ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.deepOrange,
        appBarTheme: AppBarTheme(
          systemOverlayStyle: SystemUiOverlayStyle(
            //Android 6.0+
            statusBarColor: Colors.grey[800],
            statusBarIconBrightness: Brightness.light,
            systemStatusBarContrastEnforced: false,

            //Android 8.0+
            systemNavigationBarContrastEnforced: false,
            systemNavigationBarColor: Colors.grey[800],
            systemNavigationBarIconBrightness: Brightness.light,

            //iOS
            //statusBarBrightness: Brightness.dark,
          ),
        ),
      );
}
